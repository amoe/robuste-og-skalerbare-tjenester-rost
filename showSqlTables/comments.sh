#!/bin/bash

cockroach --insecure --host=localhost sql --execute="SHOW TABLES FROM bf" | grep comments | awk '{ print $5 }'
