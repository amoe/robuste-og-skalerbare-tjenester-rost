#!/bin/bash

cockroach --insecure --host=localhost sql --execute="SHOW TABLES FROM bf" | grep posts | awk '{ print $5 }'
