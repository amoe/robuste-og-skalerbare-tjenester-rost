#!/bin/bash

cockroach --insecure --host=localhost sql --execute="SHOW TABLES FROM bf" | grep users | awk '{ print $5 }'
