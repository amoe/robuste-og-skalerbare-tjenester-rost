#!/bin/bash

cd /home/ubuntu/Robuste/robuste-og-skalerbare-tjenester-rost
git pull
cp -r /home/ubuntu/scripts  /home/ubuntu/Robuste/robuste-og-skalerbare-tjenester-rost/backup/db/

git add *
git commit -am "Backup - DB"
git push
