#!/bin/bash

cd /home/ubuntu/robuste-og-skalerbare-tjenester-rost
git pull
cp /etc/haproxy/haproxy.cfg  /home/ubuntu/robuste-og-skalerbare-tjenester-rost/backup/balancer/

git add *
git commit -am "Backup"
git push
