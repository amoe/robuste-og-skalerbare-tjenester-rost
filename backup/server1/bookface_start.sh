#!/bin/bash

echo "Bookface auto start script"

cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=192.168.133.79:26257,192.168.129.206:26257,192.168.131.7:26257 --advertise-addr=192.168.133.79:26257 --max-offset=1500ms
