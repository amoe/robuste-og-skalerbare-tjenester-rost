#!/bin/bash
. /home/ubuntu/DCSG2003_V23_group20-openrc.sh

IP1=$(openstack server list | grep backup | awk '{print $8 }' | sed 's/.*192/192/')
IP4=$(openstack server list | grep Docker | awk '{print $8 }' | sed 's/.*192/192/')
IP5=$(openstack server list | grep server3 | awk '{print $8 }' | sed 's/.*192/192/')
IP6=$(openstack server list | grep server2 | awk '{print $8 }' | sed 's/.*192/192/')
IP7=$(openstack server list | grep server1 | awk '{print $8 }' | sed 's/.*192/192/')

for ACTION in $( openstack server list | grep imt3003* | awk '{print $8, $9 }' | sed "s/.*192/192/;s/|//;s/$IP4//;s/$IP1//;s/$IP2//" ); do if nc -z "$ACTION" 22 2>/dev/null; then echo "running"; else  SERVER=$(openstack server list | grep "$ACTION" | awk '{print $4 }');   /home/ubuntu/robuste-og-skalerbare-tjenester-rost/backup/manager/scripts/discord.sh "$SERVER NOT RUNNING, REBOOTING"; openstack server start "$SERVER"; fi; done

if [ "$SERVER" = "Docker"  ] 
then 
	sleep 180
	ssh ubuntu@$IP4 "sudo docker run -d -p 10085:80 bf:v5"
	ssh ubuntu@$IP4 "sudo docker run -d -p 10086:80 bf:v5"
	ssh ubuntu@$IP4 "sudo docker run -p 11211:11211 -d 192.168.128.23:5000/memcached memcached -m 128"
fi

