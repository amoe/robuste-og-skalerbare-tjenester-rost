#!/bin/bash

# sjekk om jo er installert:

if ! which jo > /dev/null; then
sudo apt-get install jo
exit 1
fi

# hvilken URL som skal brukes. tilpass dette:
URL="http://admin:aap123@192.168.131.184:5984/clog"

# lag JSON
JSON=$( jo message="$1" date="$(date)" host=$HOSTNAME )

# send til databasen
curl -H "Content-Type: application/json" -X POST -d "$JSON" $URL
